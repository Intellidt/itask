<!DOCTYPE html>
<html>
<head>
</head>
<body>
{!! $sentBy !!} just added you to a project in iTask:
<br/>
{!! $projectName !!}
<br/>
<a href="{!! $link !!}">Accept invitation</a>
</body>
</html>
